# rest-service-example

## setup mariadb database via docker

```console
docker run --name restserviceexampledb -e MYSQL_ROOT_PASSWORD=admin -d --publish 3306:3306 --volume mariadbvolume:/var/lib/mysql mariadb:10.5.3-bionic
```

Accessing the database client via the docker image:
```console
docker exec -it restserviceexampledb bash
mysql -u root -p
Enter password: 
```

Accessing the database outside the docker image:
```
mysql -h localhost -P 3306 -u root
```

OR 

```
docker exec -it mysql_container mysql -u root -p
```

in the MariaDB console:
```console
michael@michael-pc:~$ docker exec -it restserviceexampledb mysql -u root -p
Enter password: 
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 36
Server version: 10.5.3-MariaDB-1:10.5.3+maria~bionic mariadb.org binary distribution

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

MariaDB [(none)]> select * from dual;
ERROR 1096 (HY000): No tables used
MariaDB [(none)]> select * from all_tables;
ERROR 1046 (3D000): No database selected
MariaDB [(none)]> create database test1;
Query OK, 1 row affected (0.001 sec)

MariaDB [(none)]> show databases;
+--------------------+
| Database           |
+--------------------+
| information_schema |
| mysql              |
| performance_schema |
| test1              |
+--------------------+
4 rows in set (0.004 sec)

MariaDB [(none)]> use test1;
Database changed
MariaDB [test1]> 
```

for more maria db syntax consult : https://www.tutorialspoint.com/mariadb/mariadb_create_tables.htm


```console
root@825b96c0eb6a:/etc/mysql# sed -i -e 's/0.0.0.0/127.0.0.1/g' my.cnf
root@825b96c0eb6a:/etc/mysql# while read line; do echo $line; done < my.cnf
```

Another way to connect is:
``` console
michael@michael-pc:~$ docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' restserviceexampledb
172.17.0.2
michael@michael-pc:~$ mysql -h 172.17.0.2 -u root -p
Enter password: 
Welcome to the MySQL monitor.  Commands end with ; or \g.
Your MySQL connection id is 5
Server version: 5.5.5-10.5.3-MariaDB-1:10.5.3+maria~bionic mariadb.org binary distribution

Copyright (c) 2000, 2020, Oracle and/or its affiliates. All rights reserved.

Oracle is a registered trademark of Oracle Corporation and/or its
affiliates. Other names may be trademarks of their respective
owners.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

mysql> show databases;
+--------------------+
| Database           |
+--------------------+
| information_schema |
| mysql              |
| performance_schema |
| test1              |
+--------------------+
4 rows in set (0.00 sec)

mysql> 
```

Interesting read: https://mariadb.com/kb/en/installing-and-using-mariadb-via-docker/#connecting-to-mariadb-from-outside-the-container

client for mariadb
```
docker run --name myadmin -d -e PMA_HOST=172.17.0.2 -p 9090:80 phpmyadmin/phpmyadmin

```