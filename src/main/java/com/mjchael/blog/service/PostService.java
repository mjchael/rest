package com.mjchael.blog.service;

import com.mjchael.blog.data.PostRepository;
import com.mjchael.blog.model.Post;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PostService {
    @Autowired
    private PostRepository postRepository;

    public Post getPostById(Integer postId){
        return postRepository.findById(postId).get();
    }

    public void addPost(Post post) {
        postRepository.save(post);
    }
}
