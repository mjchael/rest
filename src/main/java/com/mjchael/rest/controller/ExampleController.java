package com.mjchael.rest.controller;

import com.mjchael.blog.model.Post;
import com.mjchael.blog.service.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path="/example", produces="application/json")
public class ExampleController {

    @Autowired
    private PostService postService;

    @GetMapping("/example")
    public String example() {
        Post post = new Post();
        post.setId(1);
        postService.addPost(post);
        return postService.getPostById(1).toString();
    }
}
